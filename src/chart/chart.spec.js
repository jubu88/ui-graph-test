import Chart from './chart.js';

describe('chart', function() {
    let fakeData = JSON.parse("{\"date\":\"2018-10-30T16:12:15.138Z\",\"data\":[{\"value\":73,\"type\":0},{\"value\":36,\"type\":3},{\"value\":72,\"type\":1},{\"value\":83,\"type\":1},{\"value\":37,\"type\":3},{\"value\":43,\"type\":3},{\"value\":23,\"type\":1},{\"value\":100,\"type\":0},{\"value\":87,\"type\":2},{\"value\":56,\"type\":2},{\"value\":46,\"type\":2},{\"value\":59,\"type\":1},{\"value\":71,\"type\":3},{\"value\":29,\"type\":2},{\"value\":55,\"type\":0},{\"value\":6,\"type\":1},{\"value\":74,\"type\":3},{\"value\":71,\"type\":2},{\"value\":64,\"type\":1},{\"value\":11,\"type\":1},{\"value\":14,\"type\":1},{\"value\":91,\"type\":0},{\"value\":27,\"type\":2},{\"value\":24,\"type\":1},{\"value\":76,\"type\":0},{\"value\":49,\"type\":0},{\"value\":70,\"type\":1},{\"value\":5,\"type\":0},{\"value\":14,\"type\":2},{\"value\":78,\"type\":1},{\"value\":53,\"type\":2},{\"value\":49,\"type\":3},{\"value\":13,\"type\":1},{\"value\":55,\"type\":3},{\"value\":75,\"type\":0},{\"value\":24,\"type\":0},{\"value\":47,\"type\":2},{\"value\":63,\"type\":1},{\"value\":94,\"type\":3},{\"value\":37,\"type\":2},{\"value\":52,\"type\":2},{\"value\":84,\"type\":1},{\"value\":5,\"type\":2},{\"value\":84,\"type\":2},{\"value\":1,\"type\":2},{\"value\":41,\"type\":2},{\"value\":70,\"type\":0},{\"value\":69,\"type\":2},{\"value\":66,\"type\":1},{\"value\":46,\"type\":0},{\"value\":79,\"type\":1},{\"value\":42,\"type\":3},{\"value\":22,\"type\":1},{\"value\":10,\"type\":2},{\"value\":92,\"type\":1},{\"value\":5,\"type\":1},{\"value\":37,\"type\":3},{\"value\":71,\"type\":2},{\"value\":37,\"type\":2},{\"value\":42,\"type\":1},{\"value\":5,\"type\":0},{\"value\":1,\"type\":3},{\"value\":60,\"type\":3},{\"value\":49,\"type\":1},{\"value\":59,\"type\":3},{\"value\":87,\"type\":0},{\"value\":62,\"type\":3},{\"value\":16,\"type\":3},{\"value\":87,\"type\":3},{\"value\":98,\"type\":3},{\"value\":25,\"type\":2},{\"value\":49,\"type\":2},{\"value\":5,\"type\":3},{\"value\":7,\"type\":2},{\"value\":70,\"type\":1},{\"value\":47,\"type\":1},{\"value\":57,\"type\":1},{\"value\":30,\"type\":1},{\"value\":85,\"type\":2}]}");
    let chartInstance = new Chart(() => true, {});

    describe('updateData()', () => {
        it('add an object to each array', function () {
            chartInstance.updateData(fakeData);
            expect(chartInstance.desktopData.length).toBe(1);
            expect(chartInstance.mobileData.length).toBe(1);
            expect(chartInstance.tabletData.length).toBe(1);
        });
    });

    describe('getData()', () => {
        it('call ChartService.getData()', function () {
            chartInstance.ChartService = {
                getData: () => {
                    return {
                        then: () => {
                            return fakeData;
                        }
                    }
                }
            }
            spyOn(chartInstance.ChartService, 'getData').and.callThrough();
            chartInstance.getData();
            expect(chartInstance.ChartService.getData).toHaveBeenCalled();
        });
    });

    describe('startRender()', () => {
        it('should start interval', function () {
            chartInstance.startRender();
            expect(chartInstance.intervalObj).toBeTruthy();
        });
    });

    describe('startRender()', () => {
        it('should stop interval', function () {
            chartInstance.stopRenderer();
            expect(chartInstance.intervalObj).toBeNull();
        });
    });

    describe('reset()', () => {
        it('should stop interval', function () {
            chartInstance.reset();
            expect(chartInstance.desktopData.length).toBe(0);
            expect(chartInstance.mobileData.length).toBe(0);
            expect(chartInstance.tabletData.length).toBe(0);
        });
    });

});