import template from './chartTemplate.js';
import ChartService from './chartService.js';
import './chart.scss';

export default class Chart{

    constructor(updater, updaterContext){
        this.ChartService = new ChartService();
        this.updater = updater;
        this.updaterContext = updaterContext;
        this.name = "asd";
        this.reset();
        this.intervalObj = null;
        this.intervalTime = 2;
    }

    getData(){
        this.ChartService.getData().then((data) => data.json().then((data) => this.updateData(data)));
    }

    //Method parses data from server and updates arrays
    updateData(data){
        this.data = data;
        let type1 = {
            max: null,
            min: null,
            date: new Date(this.data.date).toLocaleTimeString()
        };
        let type2 = {
            max: null,
            min: null,
            date: new Date(this.data.date).toLocaleTimeString()
        };
        let type3 = {
            max: null,
            min: null,
            date: new Date(this.data.date).toLocaleTimeString()
        };
        this.data.data.map((item) => {
            switch(item.type){
                case 1:
                    type1.max = (type1.max < item.value || type1.max == null ? item.value : type1.max);
                    type1.min = (type1.min > item.value || type1.min == null ? item.value : type1.min);
                    break;
                case 2:
                    type2.max = (type2.max < item.value || type2.max == null ? item.value : type2.max);
                    type2.min = (type2.min > item.value || type2.min == null ? item.value : type2.min);
                    break;
                case 3:
                    type3.max = (type3.max < item.value || type3.max == null ? item.value : type3.max);
                    type3.min = (type3.min > item.value || type3.min == null ? item.value : type3.min);
                    break;
                default:
                    return null;
            }
        });
        this.desktopData.push(type1);
        this.tabletData.push(type2);
        this.mobileData.push(type3);
    }

    //Starts an interval to pull data
    startRender(){
        if(this.intervalObj)
            clearInterval(this.intervalObj);

        this.intervalObj = setInterval(() => {
            this.getData();
            this.updater('chart', this.updaterContext);
        }, this.intervalTime * 1000);
    }

    //Stops interval
    stopRenderer(){
        clearInterval(this.intervalObj);
        this.intervalObj = null;
    }

    //Resets data
    reset(){
        this.desktopData = [];
        this.mobileData = [];
        this.tabletData = [];
        this.updater('chart', this.updaterContext);
    }

    getTemplate(){
        return template(this);
    }
}