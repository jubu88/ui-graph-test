export default (chart) => `
    <div class="chartContainer desktopChart">
    ${chart.desktopData.map((item, i) => 
        `<div class="candle" style="left:${(i * 80) + 20}px; bottom: ${(item.min+25) * 2}px; height: ${(25 + item.max - item.min) * 2}px; background-color: ${(chart.desktopData[i - 1] && item.max < chart.desktopData[i - 1].max ? 'red' : 'green')}"></div>
        <div style="left:${i * 80}px; bottom: 10px;" class="candleDate desktopChart">${item.date}</div>`).join('')}
    </div>
    <div class="chartContainer tabletChart">
    ${chart.tabletData.map((item, i) =>
    `<div class="candle" style="left:${(i * 80) + 20}px; bottom: ${(item.min+25) * 2}px; height: ${(25 + item.max - item.min) * 2}px; background-color: ${(chart.tabletData[i - 1] && item.max < chart.tabletData[i - 1].max ? 'red' : 'green')}"></div>
        <div style="left:${i * 80}px; bottom: 10px;" class="candleDate tabletChart">${item.date}</div>`).join('')}
    </div>
    <div class="chartContainer mobileChart">
    ${chart.mobileData.map((item, i) =>
    `<div class="candle" style="left:${(i * 80) + 20}px; bottom: ${(item.min+25) * 2}px; height: ${(25 + item.max - item.min) * 2}px; background-color: ${(chart.mobileData[i - 1] && item.max < chart.mobileData[i - 1].max ? 'red' : 'green')}"></div>
        <div style="left:${i * 80}px; bottom: 10px;" class="candleDate mobileChart">${item.date}</div>`).join('')}
    </div>
`;
