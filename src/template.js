export default (chart) => `
 <div class="chart">
    <div class="menuBar">
        <button class="btn" id="startButton">Start</button>
        <button class="btn" id="stopButton">Stop</button>
        <button class="btn" id="resetButton">Reset</button>
        <div class="group">      
            <input type="text" id="intervalTime" value="${chart.chart.intervalTime}" required>
            <span class="highlight"></span>
            <span class="bar"></span>
            <label>Poll interval in seconds</label>
        </div>
        <button class="btn" id="changeButton">Change</button>
    </div>
    <div id="chartContainer">
    </div>
 </div>
`;
