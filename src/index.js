import Chart from './chart/chart.js';
import template from './template.js';
import './style.scss';

const default_hash = 'chart';

class Main{

    constructor(element){
        this.chart = new Chart(this.updateTemplate, this);
        this.createApp(element);
    }

    createApp(element){
        let root = document.createElement('div');
        root.setAttribute("id", "root");
        root.innerHTML = template(this);
        element.appendChild(root);
        window.onhashchange = () => this.simpleRoute();
        this.simpleRoute();
        this.setEventListeners();
    }

    simpleRoute(){
        let hash = window.location.hash;
        hash = hash.replace("#","").replace("/","");
        if(!hash)
            hash = default_hash;
        this.updateTemplate(hash, this);
    }

    updateTemplate(component, context){
        if(context[component] && context[component].getTemplate) {
            let chartContainer = document.getElementById('chartContainer');
            chartContainer.innerHTML = context[component].getTemplate();
            chartContainer.scrollLeft = chartContainer.scrollWidth - chartContainer.clientWidth;
        }
    }

    changeInterval(){
        this.chart.intervalTime = parseInt(document.getElementById('intervalTime').value);
        this.chart.startRender();
    }

    setEventListeners(){
        document.getElementById("startButton").addEventListener('click', () => this.chart.startRender());
        document.getElementById("stopButton").addEventListener('click', () => this.chart.stopRenderer());
        document.getElementById("resetButton").addEventListener('click', () => this.chart.reset());
        document.getElementById("changeButton").addEventListener('click', () => this.changeInterval());
    }

}

const app = new Main(document.body);